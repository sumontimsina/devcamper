# Devcamper API

> Backend API for DevCamper application,
> which is bootcamp directory

## Usage

Rename "config/config.env.env" to "config/config.env" and update variable/settings on your own

## Install Dependencies

```
npm install
```

## Run App

```
##Run in dev mode
npm run dev

## Run in production mode
npm start
```

-Version : 1.0.0
