const User = require("../models/User");
const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middleware/async");
const sendEmail = require("../utils/sendEmail");
const crypto = require("crypto");

//@desc Register user
//@route POST api/v1/auth/register
//@access Public

exports.registerUser = asyncHandler(async (req, res, next) => {
  const { name, email, password, role } = req.body;
  //create user
  const user = await User.create({ name, email, password, role });
  //setting cookie and sending response
  sendTokenResponse(user, 200, res);
});

//@desc Login user
//@route POST api/v1/auth/login
//@access Public

exports.loginUser = asyncHandler(async (req, res, next) => {
  const { email, password } = req.body;

  if (!email || !password) {
    return next(new ErrorResponse("Please provide Email and Password", 400));
  }
  //Validating email and password
  const user = await User.findOne({ email }).select("+password");
  //Check for user existance
  if (!user) {
    return next(new ErrorResponse("User not found with this email", 401));
  }
  //Check if password matches
  const isMatched = await user.matchPassword(password);
  if (!isMatched) {
    return next(new ErrorResponse("Invalid Password", 401));
  }
  sendTokenResponse(user, 200, res);
});

//@desc log user out / clear cookies
//@route POST api/v1/auth/logout
//@access private

exports.logout = asyncHandler(async (req, res, next) => {
  res.cookie("token", "none", {
    expires: new Date(Date.now() + 10 * 1000),
    httpOnly: true,
  });
  res.status(200).json({ success: true, user: {} });
});

//@desc get current logged user
//@route POST api/v1/auth/me
//@access private

exports.getMe = asyncHandler(async (req, res, next) => {
  const user = await User.findById(req.user.id);
  res.status(200).json({ success: true, data: user });
});

//@desc forgot Password
//@route POST api/v1/auth/forgotpassword
//@access public

exports.forgotPassword = asyncHandler(async (req, res, next) => {
  if (!req.body.email) {
    return next(new ErrorResponse("Please provide Email address", 404));
  }
  const user = await User.findOne({ email: req.body.email });
  if (!user) {
    return next(new ErrorResponse("No user accociated with this email", 404));
  }
  //Get reset password send
  const resetToken = await user.getResetPasswordToken();
  await user.save({ validateBeforeSave: false });

  //create reset url
  const resetUrl = `${req.protocol}://${req.get(
    "host"
  )}/api/v1/auth/resetPassword/${resetToken}`;

  const message = `You are receiving this email because you have requested 
  to reset password. please make a PUT request to: \n\n${resetUrl} `;

  try {
    await sendEmail({
      email: user.email,
      subject: "Password reset",
      message,
    });
    res.status(200).json({ success: true, data: "Email sent" });
  } catch (err) {
    console.log(err);
    user.getResetPasswordToken = undefined;
    user.getResetPasswordExpire = undefined;
    await user.save({ validateBeforeSave: false });

    return next(new ErrorResponse("Email could not be sent", 500));
  }
});

//@desc reset password
//@route PUT api/v1/auth/resetpassword/:resettoken
//@access public

exports.resetPassword = asyncHandler(async (req, res, next) => {
  const token = req.params.resettoken;
  const resetPasswordToken = crypto
    .createHash("sha256")
    .update(token)
    .digest("hex");
  const user = await User.findOne({
    resetPasswordToken,
    resetPasswordExpire: { $gt: Date.now() },
  });

  if (!user) {
    return next(new ErrorResponse("Invalid Token", 404));
  }
  //set new Password
  user.password = req.body.password;
  user.resetPasswordToken = undefined;
  user.resetPasswordExpire = undefined;
  await user.save({ validateBeforeSave: false });
  sendTokenResponse(user, 200, res);
});

//Get token from model, create cookie and send response
const sendTokenResponse = function (user, statusCode, res) {
  //create token
  const token = user.getSignedJwtToken();
  const options = {
    expires: new Date(
      Date.now() + process.env.JWT_COOKIE_EXPIRESIN * 24 * 60 * 60 * 1000
    ),
    httpOnly: true,
  };
  if (process.env.NODE_ENV === "production") {
    options.secure = true;
  }

  res
    .status(statusCode)
    .cookie("token", token, options)
    .json({ success: true, token });
};

//@desc update info(name,email) of  logged user
//@route PUT api/v1/auth/updatedetails
//@access private

exports.updateDetails = asyncHandler(async (req, res, next) => {
  let fieldsToUpdate = {};
  // if (!req.body.name && req.body.email) {
  //   return next(new ErrorResponse("Please at least provide name or email"));
  // }
  if (req.body.name) {
    fieldsToUpdate.name = req.body.name;
  }
  if (req.body.email) {
    fieldsToUpdate.email = req.body.email;
  }
  const user = await User.findByIdAndUpdate(req.user.id, fieldsToUpdate, {
    new: true,
    validateBeforeSave: true,
  });
  res.status(200).json({ success: true, data: user });
});

//@desc update password of  logged user
//@route PUT api/v1/auth/updatepassword
//@access private

exports.updatePassword = asyncHandler(async (req, res, next) => {
  const user = await User.findById(req.user.id).select("+password");
  if (!req.body.currentPassword) {
    return next(new ErrorResponse("Invalid current password", 401));
  }
  if (!(await user.matchPassword(req.body.currentPassword))) {
    return next(new ErrorResponse("Invalid current password", 401));
  }
  user.password = req.body.newPassword;
  await user.save();
  sendTokenResponse(user, 200, res);
});
