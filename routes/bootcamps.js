const express = require("express");
const router = express.Router();
const {
  getBootCamps,
  getBootCampsById,
  updateBootCampsById,
  deleteBootCampsById,
  createBootCamps,
  getBootCampsInRadious,
  bootcampPhotoUpload,
} = require("../controllers/bootcamps");
const Bootcamp = require("../models/Bootcamps");

const advancedResults = require("../middleware/advancedResults");
const { protect, authorize } = require("../middleware/auth");

//Include other resource router
const courseRouter = require("./courses");
const reviewRouter = require("./reviews");

//Re-route into other resource router
router.use("/:bootcampId/courses", courseRouter);
router.use("/:bootcampId/reviews", reviewRouter);

router.get("/test", (req, res, next) => {
  res.status(200).json({ message: "Bootcamp route" });
});

router.route("/radious/:zipcode/:distance").get(getBootCampsInRadious);
router
  .route("/:id/photo")
  .put(protect, authorize("publisher", "admin"), bootcampPhotoUpload);
router
  .route("/")
  .get(advancedResults(Bootcamp, "courses"), getBootCamps)
  .post(protect, authorize("publisher", "admin"), createBootCamps);
router
  .route("/:id")
  .get(getBootCampsById)
  .put(protect, authorize("publisher", "admin"), updateBootCampsById)
  .delete(protect, authorize("publisher", "admin"), deleteBootCampsById);

module.exports = router;
