const express = require("express");
const router = express.Router();
const {
  getUsers,
  getUser,
  updateUser,
  createUser,
  deleteUser,
} = require("../controllers/users");
const User = require("../models/User");
const { protect, authorize } = require("../middleware/auth");
const advancedResults = require("../middleware/advancedResults");

router.use(protect);
router.use(authorize("admin"));
router.route("/").get(advancedResults(User), getUsers).post(createUser);
router.route("/:id").put(updateUser).delete(deleteUser).get(getUser);

module.exports = router;
