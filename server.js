const express = require("express");
const dotenv = require("dotenv");
const path = require("path");
var cors = require("cors");
const colors = require("colors");
const helmet = require("helmet");
const mongoSanitize = require("express-mongo-sanitize");
const xss = require("xss-clean");
const rateLimit = require("express-rate-limit");
const hpp = require("hpp");
const cookieParser = require("cookie-parser");
const fileupload = require("express-fileupload");
const errorHandler = require("./middleware/error");
const connectDB = require("./config/db");

//Route files
const bootcamps = require("./routes/bootcamps");
const courses = require("./routes/courses");
const auth = require("./routes/auth");
const users = require("./routes/users");
const reviews = require("./routes/reviews");
const morgan = require("morgan");

//load env variables
dotenv.config({ path: "./config/config.env" });
const PORT = process.env.PORT || 5000;

//connect to database
connectDB();

const app = express();

//Body parser
app.use(express.json());
//Cookie parser
app.use(cookieParser());
//Dev logging middleware
if (process.env.NODE_ENV === "development") {
  app.use(morgan("dev"));
}

//Fileupload
app.use(fileupload());

// Mongoose sanitize
app.use(mongoSanitize());

//set security headers
app.use(helmet());

//prevent cros-ssite scripting
app.use(xss());

//rate limiting
const limiter = rateLimit({
  windowMs: 10 * 60 * 1000, // 15 minutes
  max: 100, // limit each IP to 100 requests per windowMs
});

//  apply to all requests
app.use(limiter);

//Prevent http param polution
app.use(hpp());

//Enable CORS
app.use(cors());

//set statis folder
app.use(express.static(path.join(__dirname, "public")));
//redirecting to router (mounting)
app.use("/api/v1/bootcamps", bootcamps);
app.use("/api/v1/courses", courses);
app.use("/api/v1/auth", auth);
app.use("/api/v1/users", users);
app.use("/api/v1/reviews", reviews);
app.use(errorHandler);

const server = app.listen(PORT, () => {
  console.log(
    `Server is running in  ${process.env.NODE_ENV} mode on port ${process.env.PORT}`
      .yellow.bold
  );
});

//Handle unhandled promise rejection
process.on("unhandledRejection", (err, promise) => {
  console.log(`Error : ${err.message}`.red.bold);
  //Close server and exit
  server.close(() => {
    process.exit(1);
  });
});
